<?php

namespace App\Domain\Product;

use http\Env\Request;
use Illuminate\Database\Connection;
use Psr\Http\Message\ServerRequestInterface;

class ProductRepository {
    /**
     * @var Connection
     */
    private $connection;

    /**
     * The constructor.
     *
     * @param Connection $connection The database connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    // https://odan.github.io/2019/12/03/slim4-eloquent.html
    public function find(int $productId)
    {
        $row = $this->connection->table('products')->find($productId);

        if(!$row) {
            throw new \DomainException(sprintf('Product not found: %s', $productId));
        }
        return $row;
    }

    public function all()
    {
        $row = $this->connection->table('products')->get();
        return $row;
    }

    /**
     * @param array $request
     */
    public function add(array $request)
    {
        $values = [
            'name' => $request["name"],
            'articleId' => $request["articleId"],
            'price' => $request["price"],
            'width' => $request["width"],
            'height' => $request["height"],
            'description' => $request["description"]
        ];
        $this->connection->table('products')->insert($values);
    }
}