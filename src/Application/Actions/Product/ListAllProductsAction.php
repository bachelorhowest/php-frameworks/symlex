<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use App\Application\Actions\Product\ProductAction;
use App\Domain\DomainException\DomainRecordNotFoundException;
use http\Env\Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class ListAllProductsAction extends ProductAction
{

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $product = $this->productRepository->all();
        return $this->respondWithData($product);
    }
}