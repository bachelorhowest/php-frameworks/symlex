<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use App\Application\Actions\Product\ProductAction;
use App\Domain\DomainException\DomainRecordNotFoundException;
use App\Domain\Product\ProductRepository;
use http\Env\Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;

final class AddProductAction
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository) {
        $this->productRepository = $productRepository;
    }
    public function __invoke(ServerRequestInterface $request, Response $response): Response
    {
        $params = $request->getParsedBody();
        $this->productRepository->add($params);
        $response->getBody()->write(json_encode(['success' => true]));
        return $response->withHeader('Content-Type', 'application/json');
    }
}