<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use App\Application\Actions\Product\ProductAction;
use App\Domain\DomainException\DomainRecordNotFoundException;
use http\Env\Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class ListProductsAction extends ProductAction
{

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $productId = (int) $this->resolveArg('id');
        $product = $this->productRepository->find($productId);
        return $this->respondWithData($product);
    }
}